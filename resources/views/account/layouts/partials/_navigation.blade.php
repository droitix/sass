<ul class="nav nav-pills nav-stacked flex-column">
     <li class="{{ return_if(on_page('account'), 'active') }}">
        <a href="{{ route('account.index') }}">Account overview</a>
    </li>
     <li>
        <a href="{{ route('account.profile.index') }}">Profile</a>
    </li>
    <li>
        <a href="{{ route('account.password.index') }}">Change Password</a>
    </li>
    <li class="{{ return_if(on_page('account/deactivate'), 'active') }}">
        <a href="{{ route('account.deactivate.index') }}">Deactivate account</a>
    </li>
    
</ul>    
<hr>
        <ul class="nav nav-pills nav-stacked flex-column">
            @subscribed

            @subscriptionnotcancelled
                <li>
                    <a href="{{ route('account.subscription.swap.index') }}">Change plan</a>
                </li>
                <li class="">
                    <a href="{{ route('account.subscription.cancel.index') }}">Cancel subscription</a>
                </li>
            @endsubscriptionnotcancelled
                @subscriptioncancelled
                <li>
                    <a href="{{ route('account.subscription.resume.index') }}">Resume Subscription</a>
                </li>

                @endsubscriptioncancelled
                <li class="">
                    <a href="{{ route('account.subscription.card.index') }}">Update Card</a>
                </li>
               <a href="{{ route('account.subscription.team.index') }}">Manage team</a>
                 @teamsubscription
                <li class="">
                    <a href="">Manage team</a>
                </li>
                @endteamsubscription
            @endsubscribed
            </ul>